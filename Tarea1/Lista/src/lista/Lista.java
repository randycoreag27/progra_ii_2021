package lista;

import java.util.ArrayList;
import java.util.Scanner;

public class Lista {

    public static void Turnos() {

        //Definicion de las Listas a Utilizar
        ArrayList<Integer> m = new ArrayList<Integer>();
        ArrayList<Integer> ta = new ArrayList<Integer>();

        Scanner n = new Scanner(System.in);

        //Definicion de las Variables a Utilizar
        int c1 = 0;
        int c2 = 0;
        int salario = 0;
        int total_mannana = 0;
        int total_tarde = 0;

        System.out.println("-------------------------------------");
        System.out.println("Ingrese los Salarios de los Empleados");
        System.out.println("        del Turno de la Mañana       ");
        System.out.println("-------------------------------------");

        System.out.println("");

        while (c1 < 5) {

            System.out.println("---------------------------------");
            System.out.println("Ingrese el salario del empleado: ");
            System.out.println("---------------------------------");
            salario = n.nextInt();

            m.add(salario);

            c1++;

        }

        System.out.println("-------------------------------------");
        System.out.println("Ingrese los Salarios de los Empleados");
        System.out.println("        del Turno de la Tarde        ");
        System.out.println("-------------------------------------");

        System.out.println("");

        while (c2 < 4) {

            System.out.println("---------------------------------");
            System.out.println("Ingrese el salario del empleado: ");
            System.out.println("---------------------------------");
            salario = n.nextInt();

            ta.add(salario);

            c2++;

        }

        //Ciclos para recorrer cada lista y calcular el total de gastos de cada turno
        for (int i = 0; i < m.size(); i++) {
            total_mannana += m.get(i);
        }

        for (int i = 0; i < ta.size(); i++) {
            total_tarde += ta.get(i);
        }
        
        System.out.println("");

        System.out.println("----------------------------------------------");
        System.out.println("Gasto totales de Sueldos de la Mañana: " + String.valueOf(total_mannana));
        System.out.println("----------------------------------------------");

        System.out.println("");

        System.out.println("----------------------------------------------");
        System.out.println("Gasto totales de Sueldos de la Tarde: " + String.valueOf(total_tarde));
        System.out.println("----------------------------------------------");

    }

    public static void main(String[] args) {
        Turnos();
    }

}
