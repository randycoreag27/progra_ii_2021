package matriz;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Matriz {

    public static void Notas() {

        Scanner n = new Scanner(System.in);
        Scanner n2 = new Scanner(System.in);
        Scanner t = new Scanner(System.in);

        String nombre;
        String estado="";
        double nota;
        int cantidad_estudiantes = 0;
        int c = 0;

        System.out.println("----------------------------------------------------------------");
        System.out.println("Digite el número de registros de estudiantes que desea agregar: ");
        System.out.println("----------------------------------------------------------------");
        cantidad_estudiantes = n.nextInt();

        String[][] matriz = new String[cantidad_estudiantes][3];

        for (int x = 0; x < cantidad_estudiantes; x++) {

            System.out.println("---------------------------------");
            System.out.println("Digite el nombre del Estudiante: ");
            System.out.println("---------------------------------");
            nombre = t.nextLine();

            matriz[x][0] = "Nombre: " + nombre;

            System.out.println("");

            System.out.println("-------------------------------");
            System.out.println("Digite la Nota del Estudiante: ");
            System.out.println("-------------------------------");
            nota = n2.nextDouble();
            
            
            //Condicion para validar que la nota sea entre 1 y 10
            if (nota > 10) {
                JOptionPane.showMessageDialog(null, "Error, la nota debe ser entre 0 y 10",
                        "Error", JOptionPane.ERROR_MESSAGE);
                System.out.println("-------------------------------");
                System.out.println("Digite la Nota del Estudiante: ");
                System.out.println("-------------------------------");
                nota = n.nextInt();

            } else {
                matriz[x][1] = "Nota: " + String.valueOf(nota);
            }
            
            //Condiciones para averiguar el estado en el cual se encuentra el estudiante
            if(nota>=0 && nota <=4.99){
                estado="Suspendido";
            }else if(nota>=5 && nota <=6.99){
                estado="Bien";
            }else if(nota>=7 && nota <=8.99){
                estado="Notable";
            }else if(nota>=9 && nota <=10){
                estado="Sobresaliente";
            }
            
            matriz[x][2] = "Estado: " + estado;

        }

        for (int x = 0; x < matriz.length; x++) {
            System.out.print("|");
            for (int y = 0; y < matriz[x].length; y++) {
                System.out.print(matriz[x][y]);
                if (y != matriz[x].length - 1) {
                    System.out.print("|");
                }
            }
            System.out.println("|");
        }

    }

    public static void main(String[] args) {
        Notas();
    }

}
