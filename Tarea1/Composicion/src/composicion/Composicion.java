package composicion;

import java.util.Scanner;

public class Composicion {

    public static void main(String[] args) {

        Empresa empresa = new Empresa();
        Scanner t = new Scanner(System.in);

        System.out.println("---------------------------------------------------");
        System.out.println("Digite la cantidad de empleados que desea agregar: ");
        System.out.println("---------------------------------------------------");
        int n = t.nextInt();

        for (int i = 0; i < n; i++) {
            empresa.Agregar_Empleado();
        }
        
        empresa.Consultar();

    }

}
