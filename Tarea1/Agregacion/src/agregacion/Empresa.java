package agregacion;

public class Empresa {

    //Definicion de Variables a Utilizar
    int cedula_cliente;
    String nombre_cliente;
    String numero_cliente;
    String tipo_cliente;

    //Metodo Constructor
    public Empresa() {

    }
    
    //Metodo para poder ver los clientes dentro de una matriz
    public void Ver_Clientes() {

        String[][] clientes = new String[1][4];

        for (int x = 0; x < 1; x++) {

            clientes[x][0] = "Cédula: " + String.valueOf(this.getCedula_cliente());
            clientes[x][1] = "Nombre: " + this.getNombre_cliente();
            clientes[x][2] = "Tipo de Cliente: " + this.getTipo_cliente();
            clientes[x][3] = "Número de Cliente: " + this.getNumero_cliente();

        }
        
        System.out.println("");
        System.out.println("-------------------------");
        System.out.println("----Lista de Clientes----");
        System.out.println("-------------------------");
        System.out.println("");

        for (int x = 0; x < clientes.length; x++) {
            System.out.print("|");
            for (int y = 0; y < clientes[x].length; y++) {
                System.out.print(clientes[x][y]);
                if (y != clientes[x].length - 1) {
                    System.out.print("|");
                }
            }
            System.out.println("|");
        }

    }

    //Metodos para accesos directo a los atributos
    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public String getNumero_cliente() {
        return numero_cliente;
    }

    public void setNumero_cliente(String numero_cliente) {
        this.numero_cliente = numero_cliente;
    }

    public int getCedula_cliente() {
        return cedula_cliente;
    }

    public void setCedula_cliente(int cedula_cliente) {
        this.cedula_cliente = cedula_cliente;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

}
