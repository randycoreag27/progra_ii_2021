package empleado;

public class Empleado {
    
    String nombre;
    String apellidos;
    int cedula;
    String direccion;
    int anos_laborados;
    int telefono;
    float salario;
    Empleado supervisor;
    
    public Empleado(){
        
    }
    
    public String Imprimir(){
        String i="El nombre del empleado es: "+this.getNombre();
        return i;
    }
    
    public boolean Cambiar_Supervisor(){
        Empleado lastUser= this.getSupervisor();
        this.setSupervisor(supervisor);
        if(lastUser.getNombre()!=supervisor.getNombre()){
            return true;
        }else{
            return false;
        } 
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getAnos_laborados() {
        return anos_laborados;
    }

    public void setAnos_laborados(int anos_laborados) {
        this.anos_laborados = anos_laborados;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Empleado getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Empleado supervisor) {
        this.supervisor = supervisor;
    }
    
    
}
