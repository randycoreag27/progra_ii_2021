package Acceso_Datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion_BD {

    private static String nombre_BD = "ERP_UTN";
    private static String usuario = "postgres";
    private static String contrasena = "admin123";
    private static String nombre_servidor = "localhost";
    private static String puerto = "5432";
    private static String url = "jdbc:postgresql://";
    private static boolean existe_error = false;
    private static String mensaje_error = "";
    public Connection connection = null;

    public Conexion_BD() {

        try {

            connection = DriverManager.getConnection(url + nombre_servidor + ":" + puerto + "/" + nombre_BD, usuario, contrasena);

            if (this.connection != null) {
                System.out.println("La conexión ha sido existosa");
            } else {
                existe_error = true;
                System.out.println("La conexión ha fallado");
                mensaje_error = "La conexión esta cerrada";
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        }

    }
    
    public void Desconectar(){
        try {
            this.getConnection().close();
        } catch (Exception e) {
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
