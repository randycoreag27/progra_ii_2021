package modularidad_ejercicio1;

import java.util.Scanner;
import matematicas.Exponente;

public class Modularidad_Ejercicio1 {

    public static void Usuario() {

        Scanner n = new Scanner(System.in);

        System.out.println("Digite la base del número: ");
        float base = n.nextFloat();

        System.out.println("Digite el exponente del número: ");
        float expo = n.nextFloat();

        Exponente ex = new Exponente(base, expo);

        System.out.println(ex.Calculo());

        for (int i = 0; i < 10; i++) {
            ex = new Exponente(i, 2);
            System.out.println(ex.Calculo());
        }

    }

    public static void main(String[] args) {
        Usuario();
    }

}
