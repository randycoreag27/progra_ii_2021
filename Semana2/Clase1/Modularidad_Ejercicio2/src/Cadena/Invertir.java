package Cadena;

public class Invertir {

    String cadena;
    String nueva_cadena;

    public Invertir(String c) {
        this.setCadena(c);
    }

    public Invertir() {
        
    }

    public String Invertir_Cadena() {
        
        this.setNueva_cadena("");
        for (int i = this.getCadena().length(); i > 0; i--) {
            this.setNueva_cadena(this.getNueva_cadena() + this.getCadena().charAt(i-1));

        }
        return this.getNueva_cadena();
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getNueva_cadena() {
        return nueva_cadena;
    }

    public void setNueva_cadena(String nueva_cadena) {
        this.nueva_cadena = nueva_cadena;
    }

}
