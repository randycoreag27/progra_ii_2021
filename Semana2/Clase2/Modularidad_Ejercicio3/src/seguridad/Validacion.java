package seguridad;

public class Validacion {
    
    String usuario;
    String contrasena;
    
    public Validacion(String u, String c) {
        this.setContrasena(c);
        this.setUsuario(u);
    }
    
    public boolean Validar_Acceso(){
        
        if(this.getUsuario().length()>=8 && this.getContrasena().length()>=8){
            return true;
        }else{
            return false;
        }
    }
    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    
    

}
