package transporte;

public class Vehiculo {

    String color;
    int ruedas;

    public Vehiculo() {

    }

    public Vehiculo(String co, int ru) {
        this.setColor(co);
        this.setRuedas(ru);
    }

    public String Mostrar() {

        return String.format("El color del vehiculo es: %s y la cantidad de ruedas es: %d",
                 this.getColor(), this.getRuedas());

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getRuedas() {
        return ruedas;
    }

    public void setRuedas(int ruedas) {
        this.ruedas = ruedas;
    }

}
