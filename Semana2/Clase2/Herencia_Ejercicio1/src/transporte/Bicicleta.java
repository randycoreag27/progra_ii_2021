package transporte;

public class Bicicleta extends Vehiculo {

    String tipo;

    public Bicicleta() {
        super();
    }

    public String Mostrar() {

        return super.Mostrar() + String.format(" El tipo de bicicleta es %s",
                this.getTipo());

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
