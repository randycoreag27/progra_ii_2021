package banco;

public class Cuenta_Joven extends Cuenta_Abstracta {

    String titular;
    double cantidad;
    int edad;

    public Cuenta_Joven() {

    }

    public Cuenta_Joven(String t, int e, double c) {
        this.setTitular(t);
        this.setCantidad(c);
        this.setEdad(e);
    }

    public boolean Titular_Valido() {

        if (this.getEdad() > 18 && this.getEdad() <= 25) {
            return false;
        }
        return false;
    }

    @Override
    public String Mostrar() {

        return String.format("El titular de la cuenta es %s , la edad es de %d y el monto en la cuenta es %f ",
                this.getTitular(), this.getEdad(),this.getCantidad());

    }

    @Override
    public double Ingresar_Dinero(double m) {
        this.setCantidad(this.getCantidad() + m);
        return this.getCantidad();
    }

    @Override
    public double Retirar_Dinero(double m) {

        if (this.Titular_Valido()) {
            this.setCantidad(this.getCantidad() - m);
            return this.getCantidad();
        } else {
            return this.getCantidad();
        }

    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

}
