package banco;

public class Cuenta extends Cuenta_Abstracta {

    String titular;
    double cantidad;

    public Cuenta() {

    }

    public Cuenta(String t, double c) {
        
        this.setTitular(t);
        this.setCantidad(c);

    }

    @Override
    public String Mostrar() {
        
        return String.format("El titular es %s y el monto en la cuenta es %f ", 
                this.getTitular(),this.getCantidad());
        
    }

    @Override
    public double Ingresar_Dinero(double m) {
        this.setCantidad(this.getCantidad()+m);
        return this.getCantidad();
    }

    @Override
    public double Retirar_Dinero(double m) {
        this.setCantidad(this.getCantidad()-m);
        return this.getCantidad();
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

}
