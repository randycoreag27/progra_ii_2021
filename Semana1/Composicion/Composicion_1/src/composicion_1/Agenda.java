package composicion_1;

import java.util.ArrayList;
import java.util.Scanner;

public class Agenda {

    
    private ArrayList<Contacto> contactos = new ArrayList<Contacto>();

    //Metodo constructor
    public Agenda() {

    }

    public void AgregarContacto() {

        Scanner t = new Scanner(System.in);
        Contacto contacto = new Contacto();
        System.out.println("Digite el nombre del contacto: ");
        contacto.setNombre(t.nextLine());
        System.out.println("Digite el telefono del contacto: ");
        contacto.setTelefono(t.nextLine());
        //Agregar un Usuario
        this.contactos.add(contacto);
    }

    public ArrayList<Contacto> getContactos() {
        return contactos;
    }

}
