
package composicion_1;

import java.util.Scanner;

public class Composicion_1 {

    public static void main(String[] args) {
        
        Agenda agenda = new Agenda();
        Scanner t = new Scanner(System.in);
        System.out.println("Digite la cantidad de contactos que desea agregar: ");
        int n= t.nextInt();
        for (int i = 0; i < n; i++){
            agenda.AgregarContacto();
        }
    }
    
}
