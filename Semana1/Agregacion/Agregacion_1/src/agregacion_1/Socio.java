package agregacion_1;

public class Socio {
    
    private String nombre;
    private int codigo;
    private Fecha fechaIngreso;
    
    public Socio(){
        
    }
    
    public void setFecha(Fecha fecha){
        this.fechaIngreso=fecha;
    }
    
    public void Imprimir(){
        System.out.println(String.format("El codigo es %d", this.codigo));
        System.out.println(String.format("El codigo es %s", this.nombre));
        System.out.println(String.format("El codigo es %d", this.fechaIngreso));
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    
    
}
