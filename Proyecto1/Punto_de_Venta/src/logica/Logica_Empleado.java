package logica;

import conexion.Conexion_BaseDatos;
import entidades.Empleado;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Logica_Empleado {

    private boolean existeError = false;
    private String mensajeError = "";
    Empleado emp = new Empleado();

    public boolean Agregrar_Empleado(Empleado em) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "INSERT INTO Empleado ("
                    + "Cedula" + ","
                    + "Nombre" + ","
                    + "Apellidos" + ","
                    + "Fecha_Nacimiento" + ","
                    + "Telefono" + ","
                    + "Departamento" + ")"
                    + " VALUES "
                    + "("
                    + "'" + em.getCedula() + "'" + ","
                    + "'" + em.getNombre() + "'" + ","
                    + "'" + em.getApellidos() + "'" + ","
                    + "'" + em.getFecha_nacimiento() + "'" + ","
                    + "'" + em.getTelefono() + "'" + ","
                    + "'" + em.getDepartamento() + "'" + ")";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public ArrayList Obtener_Empleado(Empleado em) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        ArrayList<String> empleados = new ArrayList<String>();

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT * FROM Empleado ORDER BY Cedula ASC";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {

                String cedula = queryResult.getString("Cedula");
                em.setCedula(cedula);

                String nombre = queryResult.getString("Nombre");
                em.setNombre(nombre);

                String apellidos = queryResult.getString("Apellidos");
                em.setApellidos(apellidos);

                String fecha_nac = queryResult.getString("Fecha_Nacimiento");
                em.setFecha_nacimiento(fecha_nac);

                String telefono = queryResult.getString("Telefono");
                em.setTelefono(telefono);

                String departamento = queryResult.getString("Departamento");
                em.setDepartamento(departamento);

                empleados.add(em.getCedula() + "#" + em.getNombre() + "#" + em.getApellidos() + "#" + em.getFecha_nacimiento()
                        + "#" + em.getTelefono() + "#" + em.getDepartamento());

            }
            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }
        return empleados;
    }

    public ArrayList<String> Obtener_NombreCompleto_Empleado() {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        ArrayList<String> nombres_completos = new ArrayList<String>();

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT Nombre, Apellidos FROM Empleado ORDER BY Cedula ASC";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {
                String nombre = queryResult.getString("Nombre");
                emp.setNombre(nombre);
                String apellidos = queryResult.getString("Apellidos");
                emp.setApellidos(apellidos);
                nombres_completos.add(emp.getNombre() + " " + emp.getApellidos());
            }

            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }

        return nombres_completos;
    }

    public boolean Eliminar_Empleado(Empleado em) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "DELETE FROM Empleado WHERE "
                    + "(Cedula=" + "'" + em.getCedula() + "'" + ") AND "
                    + "(Nombre=" + "'" + em.getNombre() + "'" + ") AND "
                    + "(Apellidos=" + "'" + em.getApellidos() + "'" + ") AND "
                    + "(Fecha_Nacimiento=" + "'" + em.getFecha_nacimiento() + "'" + ") AND "
                    + "(Telefono=" + "'" + em.getTelefono() + "'" + ") AND "
                    + "(Departamento=" + "'" + em.getDepartamento() + "'" + ");";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }

    }

    public boolean Modificar_Empleado(Empleado em) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "UPDATE Empleado SET "
                    + "Cedula=" + "'" + em.getCedula() + "'" + ","
                    + "Nombre=" + "'" + em.getNombre() + "'" + ","
                    + "Apellidos=" + "'" + em.getApellidos() + "'" + ","
                    + "Fecha_Nacimiento=" + "'" + em.getFecha_nacimiento() + "'" + ","
                    + "Telefono=" + "'" + em.getTelefono() + "'" + ","
                    + "Departamento=" + "'" + em.getDepartamento() + "'" + " "
                    + "WHERE Cedula=" + "'" + em.getCedula() + "'" + ";";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public boolean isExisteError() {
        return existeError;
    }

    public void setExisteError(boolean existeError) {
        this.existeError = existeError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

}
