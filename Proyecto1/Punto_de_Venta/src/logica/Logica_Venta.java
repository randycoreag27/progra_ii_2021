package logica;

import conexion.Conexion_BaseDatos;
import entidades.Venta;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Logica_Venta {

    private boolean existeError = false;
    private String mensajeError = "";
    

    public ArrayList Obtener_Usuario(Venta ven) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        ArrayList<String> ventas = new ArrayList<String>();

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT * FROM Ventas ORDER BY Numero_de_Venta ASC";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {

                String num_venta = queryResult.getString("Numero_de_Venta");
                ven.setNumero_venta(Integer.parseInt(num_venta));

                String fecha = queryResult.getString("Fecha");
                ven.setFecha_venta(fecha);

                String hora = queryResult.getString("Hora");
                ven.setHora_venta(hora);

                String subtotal = queryResult.getString("Subtotal");
                ven.setSubtotal(Float.parseFloat(subtotal));

                String iva = queryResult.getString("iva");
                ven.setIva(Float.parseFloat(iva));

                String total = queryResult.getString("Total");
                ven.setTotal(Float.parseFloat(total));

                ventas.add(ven.getNumero_venta() + "#" + ven.getFecha_venta() + "#"
                        + ven.getHora_venta() + "#" + ven.getSubtotal() + "#" + ven.getIva()
                        + "#" + ven.getTotal());

            }
            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }
        return ventas;
    }

    public boolean Ingresar_Venta(Venta v) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "INSERT INTO Ventas ("
                    + "Fecha" + ","
                    + "Hora" + ","
                    + "Subtotal" + ","
                    + "IVA" + ","
                    + "Total" + ")"
                    + " VALUES "
                    + "("
                    + "'" + v.getFecha_venta() + "'" + ","
                    + "'" + v.getHora_venta() + "'" + ","
                    + "'" + v.getSubtotal() + "'" + ","
                    + "'" + v.getIva() + "'" + ","
                    + "'" + v.getTotal() + "'" + ")";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public boolean isExisteError() {
        return existeError;
    }

    public void setExisteError(boolean existeError) {
        this.existeError = existeError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

}
