package logica;

import conexion.Conexion_BaseDatos;
import entidades.Usuario;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Logica_Usuario {

    private boolean existeError = false;
    private String mensajeError = "";

    public int Validar_Ingreso(Usuario u) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        int resultado = 0;

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT Usuario, Contrasenna, Tipo_Usuario FROM Usuario "
                    + "WHERE (Usuario=" + "'" + u.getUsuario() + "'" + ") AND "
                    + "(Contrasenna=" + "'" + u.getContrasenna() + "')";

            ResultSet rs = ejecucion.executeQuery(sql);

            if (rs.next()) {
                String tipo = rs.getString("Tipo_Usuario");
                u.setTipo(tipo);
                resultado = 1;
            }

            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }
        return resultado;
    }

    public boolean Agregar_Usuario(Usuario usu) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "INSERT INTO Usuario ("
                    + "Usuario" + ","
                    + "Contrasenna" + ","
                    + "Tipo_Usuario" + ","
                    + "Nombre_Empleado" + ")"
                    + " VALUES "
                    + "("
                    + "'" + usu.getUsuario() + "'" + ","
                    + "'" + usu.getContrasenna() + "'" + ","
                    + "'" + usu.getTipo() + "'" + ","
                    + "'" + usu.getNombre_empleado() + "'" + ")";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }

    }

    public ArrayList Obtener_Usuario(Usuario usu) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        ArrayList<String> usuarios = new ArrayList<String>();

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT * FROM Usuario ORDER BY Nombre_Empleado ASC";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {

                String usuario = queryResult.getString("Usuario");
                usu.setUsuario(usuario);

                String contrasenna = queryResult.getString("Contrasenna");
                usu.setContrasenna(contrasenna);

                String tipo = queryResult.getString("Tipo_Usuario");
                usu.setTipo(tipo);

                String nombre_empleado = queryResult.getString("Nombre_Empleado");
                usu.setNombre_empleado(nombre_empleado);

                usuarios.add(usu.getUsuario() + "#" + usu.getContrasenna() + "#"
                        + usu.getTipo() + "#" + usu.getNombre_empleado());

            }
            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }
        return usuarios;
    }

    public boolean Eliminar_Usuario(Usuario u) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "DELETE FROM Usuario WHERE "
                    + "(Usuario=" + "'" + u.getUsuario() + "'" + ") AND "
                    + "(Contrasenna=" + "'" + u.getContrasenna() + "'" + ") AND "
                    + "(Tipo_Usuario=" + "'" + u.getTipo() + "'" + ") AND "
                    + "(Nombre_Empleado=" + "'" + u.getTipo() + "'" + ") AND "
                    + "(Usuario=" + "'" + u.getUsuario() + "'" + ");";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }

    }

    public boolean Modificar_Usuario(Usuario u) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "UPDATE Usuario SET "
                    + "Usuario=" + "'" + u.getUsuario() + "'" + ","
                    + "Contrasenna=" + "'" + u.getContrasenna()+ "'" + ","
                    + "Tipo_Usuario=" + "'" + u.getTipo() + "'" + ","
                    + "Nombre_Empleado=" + "'" + u.getNombre_empleado() + "'" + ""
                    + "WHERE Tipo_Usuario=" + "'" + u.getTipo() + "'" + ";";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }

    }

    public boolean isExisteError() {
        return existeError;
    }

    public void setExisteError(boolean existeError) {
        this.existeError = existeError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

}
