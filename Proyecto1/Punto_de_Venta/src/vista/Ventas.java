package vista;

import entidades.Producto;
import entidades.Venta;
import logica.*;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;

public class Ventas extends javax.swing.JInternalFrame {

    Calendar fecha = new GregorianCalendar();
    Logica_Producto lp = new Logica_Producto();
    ArrayList<String> lista_productos = new ArrayList();
    Logica_Venta lv = new Logica_Venta();
    Venta ven = new Venta();
    Producto pro = new Producto();
    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");

    String pago = "";
    String fecha_compra;
    String hora_compra;
    String subtotal_compra;
    String iva_compra;
    String total_compra;

    public Ventas() {
        initComponents();
        setTitle("Ventas");
    }

    public void Realizar_Compra() {

        float p = Float.parseFloat(pago);

        float cambio = p - Float.parseFloat(label_total.getText());

        String metodo_pago = "";

        if (this.efectivo.isSelected()
                == true) {
            metodo_pago = "EFECTIVO";
        } else if (this.sinpe_movil.isSelected()
                == true) {
            metodo_pago = "SINPE MÓVIL";
        } else if (this.tarjeta.isSelected()
                == true) {
            metodo_pago = "TARJETA";
        }

        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);

        String fecha = String.valueOf(dia) + "/" + String.valueOf(mes) + "/" + String.valueOf(año);
        String h = String.valueOf(hora) + ":" + String.valueOf(minuto) + ":" + String.valueOf(segundo);

        this.label_nombre_cliente.setText(this.txt_nombre_cliente.getText());

        this.label_fecha_hora.setText(fecha + " " + h);
        this.label_me_pago.setText(metodo_pago);

        this.label_pago.setText(String.valueOf(p));
        this.label_cambio.setText(String.valueOf(cambio));

        this.label_cambio_2.setText(this.label_cambio.getText());
        this.label_pago_2.setText(this.label_pago.getText());

        ven.setFecha_venta(fecha);
        ven.setHora_venta(h);
        ven.setSubtotal(Float.parseFloat(this.label_subtotal.getText()));
        ven.setIva(Float.parseFloat(this.label_iva.getText()));
        ven.setTotal(Float.parseFloat(this.label_total.getText()));

        boolean validacion = lv.Ingresar_Venta(ven);

        if (validacion==true) {
            JOptionPane.showMessageDialog(this, "Compra Realiza Existosamente", "Exitoso",
                    JOptionPane.INFORMATION_MESSAGE);
        } else if (validacion==false) {
            JOptionPane.showMessageDialog(this, "No se pudo realizar la compra correctamente", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    public void Sacar_Montos() {

        double total;
        double subtotal = 0;
        double iva = 0;

        String monto;

        for (int i = 0; i < this.productos.getRowCount(); i++) {
            monto = (String) productos.getValueAt(i, 3);
            subtotal = subtotal + Double.parseDouble(monto);
        }

        iva = subtotal * 0.13;
        total = subtotal + iva;

        this.label_iva.setText(String.valueOf(iva));

        this.label_subtotal.setText(String.valueOf(subtotal));

        this.label_total.setText(String.valueOf(total));

        this.label_iva_2.setText(this.label_iva.getText());
        this.label_subtotal_2.setText(this.label_subtotal.getText());
        this.label_total_2.setText(this.label_total.getText());

    }

    public void Mostrar_Productos() {

        if (lista_productos.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            int t = lista_productos.size();
            String nombreC[] = {"Cant", "Nombre", "Prec_Unitario", "Monto"};
            String datos[][] = new String[t][4];
            String[] separar = new String[4];
            for (int i = 0; i < lista_productos.size(); i++) {
                separar = lista_productos.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
            }
            this.productos.setModel(new DefaultTableModel(datos, nombreC));
            this.productos.setEnabled(false);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_nombre_cliente = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txt_cantidad = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        label_cambio_2 = new javax.swing.JLabel();
        label_subtotal_2 = new javax.swing.JLabel();
        label_iva_2 = new javax.swing.JLabel();
        label_total_2 = new javax.swing.JLabel();
        efectivo = new javax.swing.JRadioButton();
        sinpe_movil = new javax.swing.JRadioButton();
        tarjeta = new javax.swing.JRadioButton();
        label_pago_2 = new javax.swing.JLabel();
        boton_imprimir = new javax.swing.JButton();
        ticekt = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        label_nombre_empleado = new javax.swing.JLabel();
        label_nombre_cliente = new javax.swing.JLabel();
        label_fecha_hora = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productos = new javax.swing.JTable();
        label_subtotal = new javax.swing.JLabel();
        label_iva = new javax.swing.JLabel();
        label_total = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        label_pago = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        label_me_pago = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        label_cambio = new javax.swing.JLabel();
        boton_hacer_comprar = new javax.swing.JButton();
        boton_annadir_producto = new javax.swing.JButton();
        boton_nueva_compra = new javax.swing.JButton();
        txt_codigo = new javax.swing.JTextField();
        boton_salir = new javax.swing.JButton();

        setBackground(new java.awt.Color(78, 78, 78));
        setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        txt_nombre_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nombre_clienteActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel18.setText("CÓDIGO PRODUCTO:");

        jLabel19.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel19.setText("NOMBRE CLIENTE:");

        jLabel20.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel20.setText("CANTIDAD: ");

        txt_cantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cantidadActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(254, 254, 254));

        jLabel54.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        jLabel54.setText("SUBTOTAL:");

        jLabel55.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        jLabel55.setText("I.V.A :");

        jLabel56.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        jLabel56.setText("TOTAL:");

        jLabel57.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        jLabel57.setText("PAGO:");

        jLabel58.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        jLabel58.setText("CAMBIO:");

        label_cambio_2.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N

        label_subtotal_2.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N

        label_iva_2.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N

        label_total_2.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N

        efectivo.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        efectivo.setText("Efectivo");

        sinpe_movil.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        sinpe_movil.setText("Sinpe Movil");
        sinpe_movil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinpe_movilActionPerformed(evt);
            }
        });

        tarjeta.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        tarjeta.setText("Tarjeta");

        label_pago_2.setFont(new java.awt.Font("Roboto Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(efectivo)
                        .addGap(18, 18, 18)
                        .addComponent(sinpe_movil)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tarjeta))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel55)
                                .addGap(18, 18, 18)
                                .addComponent(label_iva_2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel56)
                                .addGap(18, 18, 18)
                                .addComponent(label_total_2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel58)
                                .addGap(18, 18, 18)
                                .addComponent(label_cambio_2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel54)
                                .addGap(18, 18, 18)
                                .addComponent(label_subtotal_2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel57)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label_pago_2)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel54)
                    .addComponent(label_subtotal_2))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(label_iva_2))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel56)
                    .addComponent(label_total_2))
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel57)
                    .addComponent(label_pago_2))
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(label_cambio_2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(efectivo)
                    .addComponent(sinpe_movil)
                    .addComponent(tarjeta))
                .addContainerGap())
        );

        boton_imprimir.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        boton_imprimir.setText("Imprimir Factura");
        boton_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_imprimirActionPerformed(evt);
            }
        });

        ticekt.setBackground(new java.awt.Color(254, 254, 254));

        jLabel1.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel1.setText("GRUPO CYBERTOM");

        jLabel2.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel2.setText("75 METROS OESTE DEL BCR, TILARAN");

        jLabel3.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel3.setText("TELEFONO: 8310-6955 / 7296-9826");

        jLabel5.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel5.setText("EMPLEADO: ");

        jLabel6.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel6.setText("NOMBRE CLIENTE:");

        jLabel7.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel7.setText("FECHA:");

        jLabel8.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel8.setText("-------------------------------------------------------------");

        jLabel14.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel14.setText("-------------------------------------------------------------");

        jLabel15.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel15.setText("SUBTOTAL:");

        jLabel16.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel16.setText("I.V.A. :");

        jLabel17.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel17.setText("TOTAL:");

        label_nombre_empleado.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_nombre_empleado.setText("NOMBRE_EMPLEADO");

        label_nombre_cliente.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_nombre_cliente.setText("NOMBRE_CLIENTE");

        label_fecha_hora.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_fecha_hora.setText("FECHA Y HORA");

        productos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        productos.setEnabled(false);
        jScrollPane1.setViewportView(productos);

        label_subtotal.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_subtotal.setText("SUBTOTAL:");

        label_iva.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_iva.setText("I.V.A. :");

        label_total.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_total.setText("TOTAL:");

        jLabel21.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel21.setText("PAGO:");

        label_pago.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_pago.setText("PAGO");

        jLabel52.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel52.setText("MÉTODO DE PAGO:");

        label_me_pago.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_me_pago.setText("MÉTODO DE PAGO:");

        jLabel53.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabel53.setText("CAMBIO:");

        label_cambio.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        label_cambio.setText("CAMBIO");

        javax.swing.GroupLayout ticektLayout = new javax.swing.GroupLayout(ticekt);
        ticekt.setLayout(ticektLayout);
        ticektLayout.setHorizontalGroup(
            ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ticektLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ticektLayout.createSequentialGroup()
                        .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel52)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label_me_pago))
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel3)
                                    .addGroup(ticektLayout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(label_nombre_empleado))
                                    .addGroup(ticektLayout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(label_nombre_cliente))
                                    .addGroup(ticektLayout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(18, 18, 18)
                                        .addComponent(label_fecha_hora))
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel2)
                                    .addGroup(ticektLayout.createSequentialGroup()
                                        .addComponent(jLabel53)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(label_cambio))
                                    .addGroup(ticektLayout.createSequentialGroup()
                                        .addGap(99, 99, 99)
                                        .addComponent(jLabel1)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(ticektLayout.createSequentialGroup()
                        .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label_subtotal))
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label_iva))
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label_total))
                            .addGroup(ticektLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(label_pago)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        ticektLayout.setVerticalGroup(
            ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ticektLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(label_nombre_empleado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(label_nombre_cliente))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(label_fecha_hora))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(label_subtotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(label_iva))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(label_total))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(label_pago))
                .addGap(12, 12, 12)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(label_cambio)
                    .addComponent(jLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(ticektLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel52)
                    .addComponent(label_me_pago))
                .addGap(11, 11, 11))
        );

        boton_hacer_comprar.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        boton_hacer_comprar.setText("Realizar Compra");
        boton_hacer_comprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_hacer_comprarActionPerformed(evt);
            }
        });

        boton_annadir_producto.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        boton_annadir_producto.setText("Añadir Producto");
        boton_annadir_producto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_annadir_productoActionPerformed(evt);
            }
        });

        boton_nueva_compra.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        boton_nueva_compra.setText("Nueva Compra");
        boton_nueva_compra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_nueva_compraActionPerformed(evt);
            }
        });

        boton_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_salir.setForeground(new java.awt.Color(254, 254, 254));
        boton_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir_ventana.png"))); // NOI18N
        boton_salir.setText("Salir");
        boton_salir.setBorderPainted(false);
        boton_salir.setContentAreaFilled(false);
        boton_salir.setDefaultCapable(false);
        boton_salir.setFocusPainted(false);
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_cantidad, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                            .addComponent(txt_codigo)
                            .addComponent(txt_nombre_cliente)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(boton_hacer_comprar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(boton_imprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(boton_annadir_producto, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(boton_salir)
                            .addComponent(boton_nueva_compra, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ticekt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_cantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_nombre_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(boton_annadir_producto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(boton_hacer_comprar))
                    .addComponent(boton_salir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton_imprimir)
                    .addComponent(boton_nueva_compra))
                .addContainerGap())
            .addComponent(ticekt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_nombre_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nombre_clienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nombre_clienteActionPerformed

    private void txt_cantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_cantidadActionPerformed

    private void sinpe_movilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sinpe_movilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sinpe_movilActionPerformed

    private void boton_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_imprimirActionPerformed

        try {
            PrinterJob print = PrinterJob.getPrinterJob();
            print.setPrintable((Printable) this);
            boolean top = print.printDialog();
            if (top) {
                print.print();
            }
        } catch (PrinterException pex) {
            JOptionPane.showMessageDialog(this, pex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_boton_imprimirActionPerformed

    private void boton_hacer_comprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_hacer_comprarActionPerformed
        pago = JOptionPane.showInputDialog("Ingrese el Monto con el cual esta Pagando el Cliente:");
        Realizar_Compra();
    }//GEN-LAST:event_boton_hacer_comprarActionPerformed

    private void boton_annadir_productoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_annadir_productoActionPerformed

        if (this.txt_codigo.getText().equals("") || this.txt_cantidad.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "No puede dejar el campo de codigo ni el de cantidad vacios, intente"
                    + "nuevamente", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            pro.setCodigo(this.txt_codigo.getText());
            pro.setCantidad(Integer.parseInt(this.txt_cantidad.getText()));
            String producto = lp.Buscar_Producto(pro);
            if (producto.equals("Fracaso")) {
                JOptionPane.showMessageDialog(this, "Error, al conectarse a la Base de Datos", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (producto.equals("NS")) {
                JOptionPane.showMessageDialog(this, "Error, no esta disponible este articulo en la Base de Datos", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                this.lista_productos.add(producto);
                Mostrar_Productos();
                Sacar_Montos();
                this.txt_cantidad.setText("");
                this.txt_codigo.setText("");
            }
        }
    }//GEN-LAST:event_boton_annadir_productoActionPerformed

    private void boton_nueva_compraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_nueva_compraActionPerformed

        lista_productos.clear();

        this.txt_nombre_cliente.setText("");

        DefaultTableModel modelo = new DefaultTableModel();
        modelo.setRowCount(0);
        this.productos.setModel(modelo);

        this.label_nombre_cliente.setText("");

        this.label_fecha_hora.setText("");
        this.label_me_pago.setText("");

        this.label_pago.setText("");
        this.label_cambio.setText("");

        this.label_cambio_2.setText("");
        this.label_pago_2.setText("");

        this.label_iva.setText("");

        this.label_subtotal.setText("");

        this.label_total.setText("");

        this.label_iva_2.setText("");
        this.label_subtotal_2.setText("");
        this.label_total_2.setText("");

    }//GEN-LAST:event_boton_nueva_compraActionPerformed

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed

        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_boton_salirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_annadir_producto;
    private javax.swing.JButton boton_hacer_comprar;
    private javax.swing.JButton boton_imprimir;
    private javax.swing.JButton boton_nueva_compra;
    private javax.swing.JButton boton_salir;
    private javax.swing.JRadioButton efectivo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_cambio;
    private javax.swing.JLabel label_cambio_2;
    private javax.swing.JLabel label_fecha_hora;
    private javax.swing.JLabel label_iva;
    private javax.swing.JLabel label_iva_2;
    private javax.swing.JLabel label_me_pago;
    private javax.swing.JLabel label_nombre_cliente;
    private javax.swing.JLabel label_nombre_empleado;
    private javax.swing.JLabel label_pago;
    private javax.swing.JLabel label_pago_2;
    private javax.swing.JLabel label_subtotal;
    private javax.swing.JLabel label_subtotal_2;
    private javax.swing.JLabel label_total;
    private javax.swing.JLabel label_total_2;
    private javax.swing.JTable productos;
    private javax.swing.JRadioButton sinpe_movil;
    private javax.swing.JRadioButton tarjeta;
    private javax.swing.JPanel ticekt;
    private javax.swing.JTextField txt_cantidad;
    private javax.swing.JTextField txt_codigo;
    private javax.swing.JTextField txt_nombre_cliente;
    // End of variables declaration//GEN-END:variables
}
