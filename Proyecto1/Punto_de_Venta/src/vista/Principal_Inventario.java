package vista;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static vista.Punto_de_Venta.is;

public class Principal_Inventario extends javax.swing.JFrame {

    Ingresar_Productos p = new Ingresar_Productos();
    Empleados e = new Empleados();
    Usuarios u = new Usuarios();
    Ventas v = new Ventas();
    EliminarModificar_Productos produ = new EliminarModificar_Productos();
    Reporte_Empleados re = new Reporte_Empleados();
    Reporte_Usuarios ru = new Reporte_Usuarios();
    Reporte_Ventas rv = new Reporte_Ventas();
    Reporte_Productos rp = new Reporte_Productos();

    public Principal_Inventario() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escritorio = new javax.swing.JDesktopPane();
        barra_menu = new javax.swing.JMenuBar();
        menu_productos = new javax.swing.JMenu();
        submenu_ingresar = new javax.swing.JMenuItem();
        submenu_modificar_eliminar = new javax.swing.JMenuItem();
        menu_reportes = new javax.swing.JMenu();
        submenu_reporte_empleados = new javax.swing.JMenuItem();
        submenu_ventas = new javax.swing.JMenuItem();
        submenu_productos = new javax.swing.JMenuItem();
        submenu_reporte_usuarios = new javax.swing.JMenuItem();
        menu_ventas = new javax.swing.JMenu();
        menu_administracion = new javax.swing.JMenu();
        submenu_empleados = new javax.swing.JMenuItem();
        submenu_usuarios = new javax.swing.JMenuItem();
        menu_cerrar_sesion = new javax.swing.JMenu();
        menu_salir = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        escritorio.setBackground(new java.awt.Color(70, 70, 70));

        barra_menu.setBackground(new java.awt.Color(254, 254, 254));

        menu_productos.setBackground(new java.awt.Color(254, 254, 254));
        menu_productos.setForeground(new java.awt.Color(58, 58, 58));
        menu_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_producto.png"))); // NOI18N
        menu_productos.setText("Productos");
        menu_productos.setContentAreaFilled(false);
        menu_productos.setFocusable(false);
        menu_productos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        menu_productos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_productosMouseClicked(evt);
            }
        });

        submenu_ingresar.setBackground(new java.awt.Color(254, 254, 254));
        submenu_ingresar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_ingresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_agregar.png"))); // NOI18N
        submenu_ingresar.setText("Ingresar");
        submenu_ingresar.setContentAreaFilled(false);
        submenu_ingresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_ingresarActionPerformed(evt);
            }
        });
        menu_productos.add(submenu_ingresar);

        submenu_modificar_eliminar.setBackground(new java.awt.Color(254, 254, 254));
        submenu_modificar_eliminar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_modificar_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_modificar.png"))); // NOI18N
        submenu_modificar_eliminar.setText("Modificar e Eliminar");
        submenu_modificar_eliminar.setContentAreaFilled(false);
        submenu_modificar_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_modificar_eliminarActionPerformed(evt);
            }
        });
        menu_productos.add(submenu_modificar_eliminar);

        barra_menu.add(menu_productos);

        menu_reportes.setForeground(new java.awt.Color(58, 58, 58));
        menu_reportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_reportes.png"))); // NOI18N
        menu_reportes.setText("Reportes");
        menu_reportes.setContentAreaFilled(false);
        menu_reportes.setFocusable(false);
        menu_reportes.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        submenu_reporte_empleados.setBackground(new java.awt.Color(254, 254, 254));
        submenu_reporte_empleados.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_reporte_empleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_empleado.png"))); // NOI18N
        submenu_reporte_empleados.setText("Empleados");
        submenu_reporte_empleados.setEnabled(false);
        submenu_reporte_empleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                submenu_reporte_empleadosMouseClicked(evt);
            }
        });
        submenu_reporte_empleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_reporte_empleadosActionPerformed(evt);
            }
        });
        menu_reportes.add(submenu_reporte_empleados);

        submenu_ventas.setBackground(new java.awt.Color(254, 254, 254));
        submenu_ventas.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_venta_submenu.png"))); // NOI18N
        submenu_ventas.setText("Ventas");
        submenu_ventas.setEnabled(false);
        submenu_ventas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                submenu_ventasMouseClicked(evt);
            }
        });
        submenu_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_ventasActionPerformed(evt);
            }
        });
        menu_reportes.add(submenu_ventas);

        submenu_productos.setBackground(new java.awt.Color(254, 254, 254));
        submenu_productos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_productos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_submenu_producto.png"))); // NOI18N
        submenu_productos.setText("Productos");
        submenu_productos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                submenu_productosMouseClicked(evt);
            }
        });
        submenu_productos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_productosActionPerformed(evt);
            }
        });
        menu_reportes.add(submenu_productos);

        submenu_reporte_usuarios.setBackground(new java.awt.Color(254, 254, 254));
        submenu_reporte_usuarios.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_reporte_usuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_usuario_menu.png"))); // NOI18N
        submenu_reporte_usuarios.setText("Usuarios");
        submenu_reporte_usuarios.setEnabled(false);
        submenu_reporte_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_reporte_usuariosActionPerformed(evt);
            }
        });
        menu_reportes.add(submenu_reporte_usuarios);

        barra_menu.add(menu_reportes);

        menu_ventas.setForeground(new java.awt.Color(58, 58, 58));
        menu_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_venta.png"))); // NOI18N
        menu_ventas.setText("Ventas");
        menu_ventas.setContentAreaFilled(false);
        menu_ventas.setEnabled(false);
        menu_ventas.setFocusable(false);
        menu_ventas.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        menu_ventas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_ventasMouseClicked(evt);
            }
        });
        barra_menu.add(menu_ventas);

        menu_administracion.setForeground(new java.awt.Color(58, 58, 58));
        menu_administracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_administración.png"))); // NOI18N
        menu_administracion.setText("Administración");
        menu_administracion.setContentAreaFilled(false);
        menu_administracion.setEnabled(false);
        menu_administracion.setFocusable(false);
        menu_administracion.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        submenu_empleados.setBackground(new java.awt.Color(254, 254, 254));
        submenu_empleados.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_empleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_empleado.png"))); // NOI18N
        submenu_empleados.setText("Empleados");
        submenu_empleados.setContentAreaFilled(false);
        submenu_empleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_empleadosActionPerformed(evt);
            }
        });
        menu_administracion.add(submenu_empleados);

        submenu_usuarios.setBackground(new java.awt.Color(254, 254, 254));
        submenu_usuarios.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        submenu_usuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_usuario_menu.png"))); // NOI18N
        submenu_usuarios.setText("Usuarios");
        submenu_usuarios.setContentAreaFilled(false);
        submenu_usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenu_usuariosActionPerformed(evt);
            }
        });
        menu_administracion.add(submenu_usuarios);

        barra_menu.add(menu_administracion);

        menu_cerrar_sesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_cerrar_sesion.png"))); // NOI18N
        menu_cerrar_sesion.setText("Cerrar Sesión");
        menu_cerrar_sesion.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        menu_cerrar_sesion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_cerrar_sesionMouseClicked(evt);
            }
        });
        barra_menu.add(menu_cerrar_sesion);

        menu_salir.setForeground(new java.awt.Color(58, 58, 58));
        menu_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir.png"))); // NOI18N
        menu_salir.setText("Salir");
        menu_salir.setContentAreaFilled(false);
        menu_salir.setFocusable(false);
        menu_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        menu_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_salirMouseClicked(evt);
            }
        });
        barra_menu.add(menu_salir);

        setJMenuBar(barra_menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(escritorio, javax.swing.GroupLayout.PREFERRED_SIZE, 1522, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(escritorio, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submenu_ingresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_ingresarActionPerformed
        this.escritorio.add(p);
        this.p.setVisible(true);
    }//GEN-LAST:event_submenu_ingresarActionPerformed

    private void submenu_modificar_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_modificar_eliminarActionPerformed
        this.escritorio.add(produ);
        this.produ.setVisible(true);
    }//GEN-LAST:event_submenu_modificar_eliminarActionPerformed

    private void menu_productosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_productosMouseClicked

    }//GEN-LAST:event_menu_productosMouseClicked

    private void submenu_reporte_empleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_submenu_reporte_empleadosMouseClicked

    }//GEN-LAST:event_submenu_reporte_empleadosMouseClicked

    private void submenu_reporte_empleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_reporte_empleadosActionPerformed
        this.escritorio.add(re);
        this.re.setVisible(true);
    }//GEN-LAST:event_submenu_reporte_empleadosActionPerformed

    private void submenu_ventasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_submenu_ventasMouseClicked

    }//GEN-LAST:event_submenu_ventasMouseClicked

    private void submenu_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_ventasActionPerformed
        this.escritorio.add(rv);
        this.rv.setVisible(true);
    }//GEN-LAST:event_submenu_ventasActionPerformed

    private void submenu_productosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_submenu_productosMouseClicked

    }//GEN-LAST:event_submenu_productosMouseClicked

    private void submenu_productosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_productosActionPerformed
        this.escritorio.add(rp);
        this.rp.setVisible(true);
    }//GEN-LAST:event_submenu_productosActionPerformed

    private void submenu_reporte_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_reporte_usuariosActionPerformed
        this.escritorio.add(ru);
        this.ru.setVisible(true);
    }//GEN-LAST:event_submenu_reporte_usuariosActionPerformed

    private void menu_ventasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_ventasMouseClicked
        this.escritorio.add(v);
        this.v.setVisible(true);
    }//GEN-LAST:event_menu_ventasMouseClicked

    private void submenu_empleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_empleadosActionPerformed
        this.escritorio.add(e);
        this.e.setVisible(true);
    }//GEN-LAST:event_submenu_empleadosActionPerformed

    private void submenu_usuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenu_usuariosActionPerformed
        this.escritorio.add(u);
        this.u.setVisible(true);
    }//GEN-LAST:event_submenu_usuariosActionPerformed

    private void menu_cerrar_sesionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_cerrar_sesionMouseClicked
        ImageIcon icon = new ImageIcon("src/multimedia/icono_pregunta.png");
        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar sesión?", "Pregunta",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
            is.setVisible(true);
        }
    }//GEN-LAST:event_menu_cerrar_sesionMouseClicked

    private void menu_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_salirMouseClicked
        ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");
        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_menu_salirMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal_Inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra_menu;
    private javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenu menu_administracion;
    private javax.swing.JMenu menu_cerrar_sesion;
    private javax.swing.JMenu menu_productos;
    private javax.swing.JMenu menu_reportes;
    private javax.swing.JMenu menu_salir;
    private javax.swing.JMenu menu_ventas;
    private javax.swing.JMenuItem submenu_empleados;
    private javax.swing.JMenuItem submenu_ingresar;
    private javax.swing.JMenuItem submenu_modificar_eliminar;
    private javax.swing.JMenuItem submenu_productos;
    private javax.swing.JMenuItem submenu_reporte_empleados;
    private javax.swing.JMenuItem submenu_reporte_usuarios;
    private javax.swing.JMenuItem submenu_usuarios;
    private javax.swing.JMenuItem submenu_ventas;
    // End of variables declaration//GEN-END:variables
}
