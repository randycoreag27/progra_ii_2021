package vista;

import entidades.Empleado;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.Logica_Empleado;

public class Empleados extends javax.swing.JInternalFrame {

    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");
    Empleado e = new Empleado();
    Logica_Empleado le = new Logica_Empleado();
    ArrayList<String> lista_empleados = new ArrayList<String>();

    public Empleados() {
        initComponents();
        setTitle("Empleados");
        Mostrar_Empleados();
    }

    public void seleccionar_datos() {
        int c;
        c = this.empleados.getSelectedRow();
        this.txt_codigo.setText(this.empleados.getValueAt(c, 0).toString());
        this.txt_nombre.setText(this.empleados.getValueAt(c, 1).toString());
        this.txt_apellidos.setText(this.empleados.getValueAt(c, 2).toString());
        this.txt_fecha_nac.setText(this.empleados.getValueAt(c, 3).toString());
        this.txt_telefono.setText(this.empleados.getValueAt(c, 4).toString());
        this.txt_departamento.setText(this.empleados.getValueAt(c, 5).toString());
    }

    public void Mostrar_Empleados() {

        lista_empleados = le.Obtener_Empleado(e);
        int t = lista_empleados.size();

        if (lista_empleados.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String nombreC[] = {"Cédula", "Nombre", "Apellidos", "Fecha Nacimiento",
                "Télefono", "Departamento"};
            String datos[][] = new String[t][6];
            String[] separar = new String[6];
            for (int i = 0; i < lista_empleados.size(); i++) {
                separar = lista_empleados.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
                datos[i][4] = separar[4];
                datos[i][5] = separar[5];
            }
            this.empleados.setModel(new DefaultTableModel(datos, nombreC));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        boton_eliminar = new javax.swing.JButton();
        boton_salir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        txt_codigo = new javax.swing.JTextField();
        txt_nombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_apellidos = new javax.swing.JTextField();
        txt_departamento = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_fecha_nac = new javax.swing.JFormattedTextField();
        txt_telefono = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        empleados = new javax.swing.JTable();
        boton_agregar = new javax.swing.JButton();
        boton_modificar = new javax.swing.JButton();

        setBackground(new java.awt.Color(78, 78, 78));
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setFocusable(false);
        setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(254, 254, 254));
        jLabel1.setText("Empleados");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(254, 254, 254));
        jLabel3.setText("Registrode Empleados");

        boton_eliminar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_eliminar.setForeground(new java.awt.Color(254, 254, 254));
        boton_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/sacar_producto.png"))); // NOI18N
        boton_eliminar.setText("Eliminar");
        boton_eliminar.setBorderPainted(false);
        boton_eliminar.setContentAreaFilled(false);
        boton_eliminar.setDefaultCapable(false);
        boton_eliminar.setFocusPainted(false);
        boton_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_eliminarActionPerformed(evt);
            }
        });

        boton_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_salir.setForeground(new java.awt.Color(254, 254, 254));
        boton_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir_ventana.png"))); // NOI18N
        boton_salir.setText("Salir");
        boton_salir.setBorderPainted(false);
        boton_salir.setContentAreaFilled(false);
        boton_salir.setDefaultCapable(false);
        boton_salir.setFocusPainted(false);
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(144, 144, 144));

        txt_codigo.setBackground(new java.awt.Color(60, 60, 60));
        txt_codigo.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        txt_codigo.setForeground(new java.awt.Color(254, 254, 254));
        txt_codigo.setMinimumSize(new java.awt.Dimension(17, 35));

        txt_nombre.setBackground(new java.awt.Color(60, 60, 60));
        txt_nombre.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        txt_nombre.setForeground(new java.awt.Color(254, 254, 254));
        txt_nombre.setMinimumSize(new java.awt.Dimension(17, 35));

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(254, 254, 254));
        jLabel2.setText("Cédula:");

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(254, 254, 254));
        jLabel4.setText("Nombre:");

        jLabel5.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(254, 254, 254));
        jLabel5.setText("Apellidos:");

        jLabel6.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(254, 254, 254));
        jLabel6.setText("Fecha Nacimiento:");

        txt_apellidos.setBackground(new java.awt.Color(60, 60, 60));
        txt_apellidos.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        txt_apellidos.setForeground(new java.awt.Color(254, 254, 254));
        txt_apellidos.setMinimumSize(new java.awt.Dimension(17, 35));

        txt_departamento.setBackground(new java.awt.Color(60, 60, 60));
        txt_departamento.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        txt_departamento.setForeground(new java.awt.Color(254, 254, 254));
        txt_departamento.setMinimumSize(new java.awt.Dimension(17, 35));

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(254, 254, 254));
        jLabel7.setText("Telefono:");

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(254, 254, 254));
        jLabel8.setText("Departamento:");

        txt_fecha_nac.setBackground(new java.awt.Color(60, 60, 60));
        txt_fecha_nac.setForeground(new java.awt.Color(254, 254, 254));
        try {
            txt_fecha_nac.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txt_fecha_nac.setPreferredSize(new java.awt.Dimension(17, 35));

        txt_telefono.setBackground(new java.awt.Color(60, 60, 60));
        txt_telefono.setForeground(new java.awt.Color(254, 254, 254));
        try {
            txt_telefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txt_telefono.setPreferredSize(new java.awt.Dimension(17, 35));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_departamento, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(96, 96, 96)
                        .addComponent(txt_telefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_apellidos, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                                    .addComponent(txt_nombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_codigo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_fecha_nac, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(txt_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_nombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_apellidos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_fecha_nac, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addComponent(txt_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_departamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(17, 17, 17))
        );

        empleados.setBackground(new java.awt.Color(144, 144, 144));
        empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        empleados.setRowHeight(20);
        empleados.setRowMargin(5);
        empleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                empleadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(empleados);

        boton_agregar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_agregar.setForeground(new java.awt.Color(254, 254, 254));
        boton_agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_agregar.png"))); // NOI18N
        boton_agregar.setText("Agregar");
        boton_agregar.setBorderPainted(false);
        boton_agregar.setContentAreaFilled(false);
        boton_agregar.setDefaultCapable(false);
        boton_agregar.setFocusPainted(false);
        boton_agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_agregarActionPerformed(evt);
            }
        });

        boton_modificar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_modificar.setForeground(new java.awt.Color(254, 254, 254));
        boton_modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_modificar_boton.png"))); // NOI18N
        boton_modificar.setText("Modificar");
        boton_modificar.setBorderPainted(false);
        boton_modificar.setContentAreaFilled(false);
        boton_modificar.setDefaultCapable(false);
        boton_modificar.setFocusPainted(false);
        boton_modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_modificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(92, 92, 92)
                        .addComponent(boton_agregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(boton_modificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(boton_eliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(boton_salir))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel1)
                        .addGap(6, 6, 6)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(boton_eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(boton_salir)
                                .addComponent(boton_modificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(boton_agregar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_eliminarActionPerformed
        int c;
        try {
            c = this.empleados.getSelectedRow();
            e.setCedula(this.empleados.getValueAt(c, 0).toString());
            e.setNombre(this.empleados.getValueAt(c, 1).toString());
            e.setApellidos(this.empleados.getValueAt(c, 2).toString());
            e.setFecha_nacimiento(this.empleados.getValueAt(c, 3).toString());
            e.setTelefono(this.empleados.getValueAt(c, 4).toString());
            e.setDepartamento(this.empleados.getValueAt(c, 5).toString());

            boolean validacion = le.Eliminar_Empleado(e);

            if (validacion == true) {
                JOptionPane.showMessageDialog(this, "Datos Eliminados Correctamente", "Exitoso", JOptionPane.INFORMATION_MESSAGE);
                Mostrar_Empleados();
            } else {
                JOptionPane.showMessageDialog(this, "Error no se pudo eliminar el registro",
                        "Error", JOptionPane.ERROR_MESSAGE, icon);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Para eliminar el registro debes primero seleccionar una fila de la tabla",
                    "Error", JOptionPane.ERROR_MESSAGE, icon);
        }
    }//GEN-LAST:event_boton_eliminarActionPerformed

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed

        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_boton_salirActionPerformed

    private void boton_agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_agregarActionPerformed
        if (this.txt_codigo.getText().equals("") || this.txt_nombre.getText().equals("")
                || this.txt_apellidos.getText().equals("") || this.txt_fecha_nac.getText().equals("")
                || this.txt_telefono.getText().equals("") || this.txt_departamento.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "No puede dejar ningun campo vacío", "Error",
                    JOptionPane.ERROR_MESSAGE, icon);
        } else {
            e.setCedula(this.txt_codigo.getText());
            e.setNombre(this.txt_nombre.getText());
            e.setApellidos(this.txt_apellidos.getText());
            e.setTelefono(this.txt_telefono.getText());
            e.setFecha_nacimiento(this.txt_fecha_nac.getText());
            e.setDepartamento(this.txt_departamento.getText());
            boolean validacion = le.Agregrar_Empleado(e);
            if (validacion == true) {
                JOptionPane.showMessageDialog(this, "Se guardarón correctamente los datos", "Exitoso",
                        JOptionPane.INFORMATION_MESSAGE);
                this.txt_codigo.setText("");
                this.txt_nombre.setText("");
                this.txt_apellidos.setText("");
                this.txt_fecha_nac.setText("");
                this.txt_telefono.setText("");
                this.txt_departamento.setText("");
                this.txt_codigo.requestFocus();
                Mostrar_Empleados();
            } else {
                JOptionPane.showMessageDialog(this, "No se pudo guardar los datos en la Base de Datos", "Error",
                        JOptionPane.ERROR_MESSAGE, icon);
            }
        }

    }//GEN-LAST:event_boton_agregarActionPerformed

    private void boton_modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_modificarActionPerformed

        try {
            if (this.txt_codigo.getText().equals("") || this.txt_nombre.getText().equals("")
                    || this.txt_apellidos.getText().equals("") || this.txt_fecha_nac.getText().equals("")
                    || this.txt_telefono.getText().equals("") || this.txt_departamento.getText().equals("")) {
                JOptionPane.showMessageDialog(this, "No puede dejar ningun campo vacío", "Error",
                        JOptionPane.ERROR_MESSAGE, icon);
            } else {
                e.setCedula(this.txt_codigo.getText());
                e.setNombre(this.txt_nombre.getText());
                e.setApellidos(this.txt_apellidos.getText());
                e.setTelefono(this.txt_telefono.getText());
                e.setFecha_nacimiento(this.txt_fecha_nac.getText());
                e.setDepartamento(this.txt_departamento.getText());
                boolean validacion = le.Modificar_Empleado(e);
                if (validacion == true) {
                    JOptionPane.showMessageDialog(this, "Se guardarón correctamente los datos", "Exitoso",
                            JOptionPane.INFORMATION_MESSAGE);
                    this.txt_codigo.setText("");
                    this.txt_nombre.setText("");
                    this.txt_apellidos.setText("");
                    this.txt_fecha_nac.setText("");
                    this.txt_telefono.setText("");
                    this.txt_departamento.setText("");
                    this.txt_codigo.requestFocus();
                    Mostrar_Empleados();
                } else {
                    JOptionPane.showMessageDialog(this, "No se pudo guardar los datos en la Base de Datos", "Error",
                            JOptionPane.ERROR_MESSAGE, icon);
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Para eliminar el registro debes primero seleccionar una fila de la tabla",
                    "Error", JOptionPane.ERROR_MESSAGE, icon);
        }

    }//GEN-LAST:event_boton_modificarActionPerformed

    private void empleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_empleadosMouseClicked
        seleccionar_datos();
    }//GEN-LAST:event_empleadosMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_agregar;
    private javax.swing.JButton boton_eliminar;
    private javax.swing.JButton boton_modificar;
    private javax.swing.JButton boton_salir;
    private javax.swing.JTable empleados;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txt_apellidos;
    private javax.swing.JTextField txt_codigo;
    private javax.swing.JTextField txt_departamento;
    private javax.swing.JFormattedTextField txt_fecha_nac;
    private javax.swing.JTextField txt_nombre;
    private javax.swing.JFormattedTextField txt_telefono;
    // End of variables declaration//GEN-END:variables
}
