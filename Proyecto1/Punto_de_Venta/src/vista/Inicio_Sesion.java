package vista;

import entidades.Usuario;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import logica.Logica_Usuario;
import static vista.Punto_de_Venta.pi;
import static vista.Punto_de_Venta.ps;
import static vista.Punto_de_Venta.pv;

public class Inicio_Sesion extends javax.swing.JFrame {

    ImageIcon icono_error = new ImageIcon("src/multimedia/icono_salir.png");
    ImageIcon icono_soporte = new ImageIcon("src/multimedia/usuario_soporte.png");
    Logica_Usuario lu = new Logica_Usuario();
    Usuario u = new Usuario();

    public Inicio_Sesion() {
        initComponents();
        setTitle("Inicio Sesión");
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_usuario = new javax.swing.JTextField();
        txt_contra = new javax.swing.JPasswordField();
        boton_iniciar_sesion = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(58, 58, 58));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/imagen_login.png"))); // NOI18N

        jLabel4.setFont(new java.awt.Font("Roboto Black", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(254, 254, 254));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("INICIAR SESIÓN");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        txt_usuario.setBackground(new java.awt.Color(254, 254, 254));
        txt_usuario.setFont(new java.awt.Font("Roboto Black", 1, 18)); // NOI18N

        txt_contra.setBackground(new java.awt.Color(254, 254, 254));
        txt_contra.setFont(new java.awt.Font("Roboto Black", 1, 18)); // NOI18N

        boton_iniciar_sesion.setBackground(new java.awt.Color(254, 254, 254));
        boton_iniciar_sesion.setFont(new java.awt.Font("Roboto Black", 1, 14)); // NOI18N
        boton_iniciar_sesion.setText("INICIAR SESIÓN");
        boton_iniciar_sesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_iniciar_sesionActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_contraseña.png"))); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_usuario.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_usuario)
                    .addComponent(txt_contra)
                    .addComponent(boton_iniciar_sesion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                .addGap(3, 3, 3)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(15, 15, 15))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_contra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(boton_iniciar_sesion)
                .addGap(19, 19, 19))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_iniciar_sesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_iniciar_sesionActionPerformed

        if (this.txt_usuario.getText().equals("") || this.txt_contra.getPassword().equals("")) {
            JOptionPane.showMessageDialog(this, "No puede dejar ningun campo vacío", "Error",
                    JOptionPane.ERROR_MESSAGE, icono_error);
        } else {

            u.setUsuario(this.txt_usuario.getText());
            u.setContrasenna(String.valueOf(this.txt_contra.getPassword()));

            int resultado = lu.Validar_Ingreso(u);

            if (resultado == 1) {
                if (u.getTipo().equals("Soporte")) {
                    this.dispose();
                    ps.setVisible(true);
                    JOptionPane.showMessageDialog(this, "Bienvenido su rol es de: " + u.getTipo(), "Bienvenido",
                            JOptionPane.INFORMATION_MESSAGE, this.icono_soporte);
                    this.txt_contra.setText("");
                    this.txt_usuario.setText("");
                } else if (u.getTipo().equals("Ventas")) {
                    this.dispose();
                    pv.setVisible(true);
                    JOptionPane.showMessageDialog(this, "Bienvenido su rol es de: " + u.getTipo(), "Bienvenido",
                            JOptionPane.INFORMATION_MESSAGE, this.icono_soporte);
                    this.txt_contra.setText("");
                    this.txt_usuario.setText("");
                } else if (u.getTipo().equals("Inventario")) {
                    this.dispose();
                    pi.setVisible(true);
                    JOptionPane.showMessageDialog(this, "Bienvenido su rol es de: " + u.getTipo(), "Bienvenido",
                            JOptionPane.INFORMATION_MESSAGE, this.icono_soporte);
                    this.txt_contra.setText("");
                    this.txt_usuario.setText("");
                }

            } else {
                JOptionPane.showMessageDialog(this, "Error usuario o contraseña incorrectos", "Error",
                        JOptionPane.ERROR_MESSAGE, icono_error);
                this.txt_contra.setText("");
                this.txt_usuario.setText("");
                this.txt_usuario.requestFocus();
            }
        }

    }//GEN-LAST:event_boton_iniciar_sesionActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Inicio_Sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Inicio_Sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Inicio_Sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Inicio_Sesion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio_Sesion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_iniciar_sesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField txt_contra;
    private javax.swing.JTextField txt_usuario;
    // End of variables declaration//GEN-END:variables
}
