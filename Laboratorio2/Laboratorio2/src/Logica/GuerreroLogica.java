package Logica;

import AccesoDatos.GuerreroArchivo;
import Entidad.Guerrero;

public class GuerreroLogica {

    private boolean hayError;
    private String mensajeError;

    public boolean isHayError() {
        return hayError;
    }

    public void setHayError(boolean hayError) {
        this.hayError = hayError;
    }


    public String getMensajeError() {
        return mensajeError;
    }


    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public Boolean Agregar(Guerrero guerrero) {

        GuerreroArchivo oGuerreroArchivo = new GuerreroArchivo();
        this.setHayError(oGuerreroArchivo.Agregar(guerrero));
        if (!this.isHayError()) {
            this.setMensajeError("No se agrego el mago");
        }
        return this.isHayError();
    }
}
