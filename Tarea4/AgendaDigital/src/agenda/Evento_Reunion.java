package agenda;

import static vista.Main.lista_evento_reunion;

public class Evento_Reunion extends Eventos {

    private String tipo_de_reunion;

    public Evento_Reunion() {

    }

    @Override
    public boolean Agregar_Evento() {
        lista_evento_reunion.add(this.getTipo_de_reunion() + "@" + this.getDescripcion() + "@"
                + this.getLugar() + "@" + this.getFecha() + "@" + this.getHora());

        System.out.println(lista_evento_reunion);

        if (lista_evento_reunion.isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }

    public String getTipo_de_reunion() {
        return tipo_de_reunion;
    }

    public void setTipo_de_reunion(String tipo_de_reunion) {
        this.tipo_de_reunion = tipo_de_reunion;
    }

}
