package agenda;

import static vista.Main.lista_contacto_familiar;

public class Contacto_Familiar extends Contactos {

    private String parentesco;

    public Contacto_Familiar() {

    }

    @Override
    public boolean Agregar_Contacto() {
        lista_contacto_familiar.add(this.getNombre() + "#"
                + this.getApellidos() + "#" + this.getTelefono() + "#"
                + this.getFecha_nacimiento() + "#" + this.getDireccion() + "#"
                + this.getCorreo_electronico() + "#" + this.getParentesco()); 
        if (lista_contacto_familiar.isEmpty() == true) {
            System.out.println(lista_contacto_familiar);
            return false;
        } else {
            System.out.println(lista_contacto_familiar);
            return true;
        }
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

}
