package agenda;

import static vista.Main.lista_contacto_amistad;

public class Contacto_Amigo extends Contactos {

    private String lugar_de_conocimiento;

    public Contacto_Amigo() {

    }

    @Override
    public boolean Agregar_Contacto() {
        lista_contacto_amistad.add(this.getNombre() + "#"
                + this.getApellidos() + "#" + this.getTelefono() + "#"
                + this.getFecha_nacimiento() + "#" + this.getDireccion() + "#"
                + this.getCorreo_electronico() + "#" + this.getLugar_de_conocimiento());

        System.out.println(lista_contacto_amistad);

        if (lista_contacto_amistad.isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }

    public String getLugar_de_conocimiento() {
        return lugar_de_conocimiento;
    }

    public void setLugar_de_conocimiento(String lugar_de_conocimiento) {
        this.lugar_de_conocimiento = lugar_de_conocimiento;
    }

}
