package agenda;

import static vista.Main.lista_contacto_empresarial;

public class Contacto_Empresarial extends Contactos {

    private String nombre_empresa;
   
    public Contacto_Empresarial() {

    }

    @Override
    public boolean Agregar_Contacto() {
        lista_contacto_empresarial.add(this.getNombre() + "#"
                + this.getApellidos() + "#" + this.getTelefono() + "#"
                + this.getFecha_nacimiento() + "#" + this.getDireccion() + "#"
                + this.getCorreo_electronico() + "#" + this.getNombre_empresa());

        System.out.println(lista_contacto_empresarial);

        if (lista_contacto_empresarial.isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

}
