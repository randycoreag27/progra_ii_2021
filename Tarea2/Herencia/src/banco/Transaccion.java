package banco;

import java.util.Date;

public class Transaccion {
    
    String nombre_titular;
    String id_titular;
    Date fecha_transaccion;
    int numero_transaccion;
    
    public Transaccion(){
        
    }
    
    public void hacer_transaccion(){
        
    }
    
    public void confirmar_transaccion(){
         
    }

    public String getNombre_titular() {
        return nombre_titular;
    }

    public void setNombre_titular(String nombre_titular) {
        this.nombre_titular = nombre_titular;
    }

    public String getId_titular() {
        return id_titular;
    }

    public void setId_titular(String id_titular) {
        this.id_titular = id_titular;
    }

    public Date getFecha_transaccion() {
        return fecha_transaccion;
    }

    public void setFecha_transaccion(Date fecha_transaccion) {
        this.fecha_transaccion = fecha_transaccion;
    }

    public int getNumero_transaccion() {
        return numero_transaccion;
    }

    public void setNumero_transaccion(int numero_transaccion) {
        this.numero_transaccion = numero_transaccion;
    }
    
    
    
    
    
}
