package federacion;

public class Futbolista extends Seleccion_Futbol {

    int dorsal;
    String posicion;

    public Futbolista() {

    }

    public void Entrevista() {

    }

    @Override
    public void Viajar() {
    }

    @Override
    public void Concentrarse() {
    }

    @Override
    public void Entranamientos() {
    }

    @Override
    public void Partido() {
    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

}
